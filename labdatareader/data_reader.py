"""_____________________________________________________________________

:PROJECT: LabDataReader

*data reader base class*

:details: data reader base class
          and interface to read a data file

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201025

________________________________________________________________________

"""

__version__ = "0.0.4"


import os
import sys
import pkgutil
import importlib
import logging

from pathlib import Path
from collections import namedtuple
from typing import Optional
import pandas as pd

import labdatareader.methods
from labdatareader.data_descriptor import DescriptionFormat as desf


class DataReader:
    """DataReader interface class.
    Create an instance of this class to read supported data formats.

    """

    DataFormatID = namedtuple("DataFormatID", ['pkg_name', 'methods', 'method',
                                               'data_format_class', 'data_format_subclass'], defaults='')

    def __init__(self,
                 method: str = "absorbance",
                 data_format: str = "generic.csv",
                 auto_detect: bool = False,
                 data_path: str = "",
                 filename_pattern: str = None,
                 header_map: Optional[dict] = None,
                 metadata_defaults: dict = {},
                 data_defaults: dict = {},
                 data_start_ends: Optional[list] = [(0, 0)]):
        """DataReader constructor.

        Note: character encoding is recognised and converted automatically in most cases.

        :param method: (Physical) measuring methon, like "absorbance, HPLC, NMR"
        :type method: str
        :param data_format: detailed specification of data format
          like "BMGomega.uv_vis", (use list_data_format_subclasses method
          to get a complete list of currently supported formats ) defaults to "generic.csv"
        :type data_format: str, optional
        :param data_path: path to data files, defaults to ""
        :type data_path: str, optional
        :param filename_pattern: filename (shell) pattern, defaults to "*.xml"
        :type filename_pattern: str, optional
        :param header_map: mapping to standard header for generic reader, defaults to None
        :type header_map: Optional[dict], optional
        :param metadata_defaults: default values for metadata, defaults to None
        :type metadata_defaults: Optional[dict], optional
        :param data_start_ends: skip n lines before header/data in generic reader, starting from 0, defaults to None
        :type data_start_ends: Optional[list], optional
        """

        self.data_format = data_format
        self.method = method
        self.data_path = data_path
        self._filename_pattern = filename_pattern
        self._data_start_ends_list = data_start_ends
        self._header_map = header_map
        self._metadata_defaults = metadata_defaults
        self._data_defaults = data_defaults

        self._data_filename_list = []
        self.discovered_plugins = {}

        self.implementation = None

        # register data readers
        # use plugin system -> dictionary of format_class.device_name (s. lara) and ReaderClass
        self._register_all_data_formats(labdatareader.methods)

        # making elements addressable by name
        self.set_data_format(method, data_format)

        if filename_pattern is not None:
            self.set_filename_pattern()

    def _register_all_data_formats(self, namespace_pkg):
        """
        recursively iterate through namespace
        Specifying the second argument (prefix) to iter_modules makes the
        returned name an absolute name instead of a relative one. This allows
        import_module to work without having to do additional modification to
        the name.
        s. https://packaging.python.org/guides/creating-and-discovering-plugins/
        """

        for finder, name, ispkg in pkgutil.iter_modules(namespace_pkg.__path__, namespace_pkg.__name__ + "."):
            submodule = importlib.import_module(name)
            if ispkg:
                self._register_all_data_formats(submodule)
            elif 'descriptor' not in name:
                data_format_id = DataReader.DataFormatID(*name.split('.'))
                self.discovered_plugins[data_format_id] = submodule

    @property
    def filename_pattern(self):
        return self._filename_pattern

    def set_filename_pattern(self, filename_pattern: str = None):
        """
        docstring
        """
        if filename_pattern is not None:
            self._filename_pattern = filename_pattern

        try:
            self._data_filename_list = [str(filename) for filename
                                        in Path(self.data_path).glob(self._filename_pattern)]
        except AttributeError as err:
            sys.stderr.write(
                f"{err}: no files of pattern [{self._filename_pattern}] found in \n {self.data_path}\n")

        if len(self._data_filename_list) == 0:
            raise NoDataFileError(
                f"no files with pattern [{self._filename_pattern}] found in \n {self.data_path}\n.")
        else:
            self.set_filename_list()

    @property
    def filename_list(self):
        if self._data_filename_list is not None:
            return self._data_filename_list

    def set_filename_list(self, data_filename_list: list = None):
        if data_filename_list is not None:
            self._data_filename_list = [os.path.join(
                self.data_path, fn) for fn in data_filename_list]

        # dependency injection to allow different reader implementations
        try:
            self.implementation = self.discovered_plugins[self.data_format_id].DataReaderImpl(data_path=self.data_path,
                                                                                              data_filename_list=self._data_filename_list,
                                                                                              header_map=self._header_map,
                                                                                              metadata_defaults=self._metadata_defaults,
                                                                                              data_defaults=self._data_defaults,
                                                                                              data_start_ends=self._data_start_ends_list
                                                                                              )
        except DataFormatIdError as err:
            logging.error(
                f"DataFormatIdError: No dataformat with this ID {err} found !")

    def set_data_format(self, method: str, data_format: str):
        """
        docstring
        """
        self.method = method
        self.data_format = data_format

        self.data_format_id = DataReader.DataFormatID('labdatareader', 'methods',
                                                      method, *data_format.split('.'))

    @property
    def data_start_ends(self):
        """
        docstring
        """
        return self._data_start_ends_list

    @data_start_ends.setter
    def data_start_ends(self, start_ends: list):
        """
        docstring
        """
        self._data_start_ends_list = start_ends
        if self.implementation is not None:
            self.implementation.data_start_ends = start_ends

    @property
    def header_map(self):
        """
        docstring
        """
        return self._header_map

    @header_map.setter
    def header_map(self, header_map: dict):
        """
        docstring
        """
        self._header_map = header_map
        if self.implementation is not None:
            self.implementation.header_map = header_map

    @property
    def metadata_defaults(self):
        """
        docstring
        """
        return self._metadata_defaults

    @metadata_defaults.setter
    def metadata_defaults(self, metadata_defaults: dict):
        """
        docstring
        """
        self._metadata_defaults = metadata_defaults
        if self.implementation is not None:
            self.implementation.metadata_defaults = metadata_defaults

    @property
    def data_defaults(self):
        """
        docstring
        """
        return self._data_defaults

    @data_defaults.setter
    def data_defaults(self, data_defaults: dict):
        """
        docstring
        """
        self._data_defaults = data_defaults
        if self.implementation is not None:
            self.implementation.data_defaults = data_defaults

    @property
    def csv_delimiter(self):
        """
        docstring
        """
        if self.implementation is not None:
            return self.implementation.csv_delimiter

    @csv_delimiter.setter
    def csv_delimiter(self, delimiter: str):
        """
        docstring
        """
        if self.implementation is not None:
            self.implementation.csv_delimiter = delimiter

    # quering file formats and method-classes

    @property
    def supported_methods(self):
        """List (physical methods)

        :return: [description]
        :rtype: [type]
        """
        return {plugin.method for plugin in self.discovered_plugins.keys()}

    @property
    def supported_data_format_classes(self):
        """List data format classes, in most cases the software that generated the data 

        :return: [description]
        :rtype: [type]
        """
        return [plugin.data_format_class for plugin in self.discovered_plugins.keys()]

    @property
    def supported_data_format_subclasses(self):
        """List data format sub-classes

        :return: [description]
        :rtype: [type]
        """
        return [plugin.data_format_subclass for plugin in self.discovered_plugins.keys()]

    @property
    def supported_data_formats(self):
        """
        List supported data formats.
        Recursively iterate through methods directory tree and search for plugins.
        """
        return [f"{plugin.method}.{plugin.data_format_class}.{plugin.data_format_subclass}"
                for plugin in self.discovered_plugins.keys()]

    @property
    def data_file_list(self) -> list:
        """Provides the list of files matching the file pattern provided by the constructor.

        :return: list of files matching the file pattern provided by the constructor.
        :rtype: list
        """

        if len(self._data_filename_list) > 0:
            return self._data_filename_list

    @property
    def dataframe(self):
        if self.implementation is not None:
            return self.implementation.dataframe

    @property
    def dataframe_by_well(self):
        if self.implementation is not None:
            return self.implementation.dataframe_by_well

    @property
    def dataframe_by_time(self, time_unit_factor: float = None):
        if self.implementation is not None:
            if time_unit_factor is None:
                return self.implementation.dataframe_by_time
            else:
                df = self.implementation.dataframe_by_time
                df["delta_time_unit"] = df["delta_datetime"].apply(
                    lambda delta_datetime: (delta_datetime.total_seconds() * time_unit_factor))
                return df

    def dataframe_by_timeunit(self, time_unit_factor: float = 1.0):
        if self.implementation is not None:
            df = self.implementation.dataframe_by_time
            df["delta_time_unit"] = df["delta_datetime"].apply(
                lambda delta_datetime: (delta_datetime.total_seconds() / time_unit_factor))
            return df

    def describe_method(self, method: str = None, descr_format: desf = desf.MD):
        if self.implementation is not None:
            return self.implementation.describe_method(method, descr_format)

    def describe_data_format(self, method_data_format: str = None, descr_format: desf = desf.MD):
        data_format_id = DataReader.DataFormatID('labdatareader', 'methods',
                                                 *method_data_format.split('.'))
        return self.discovered_plugins[data_format_id].DataReaderImpl.descriptor.description(descr_format)

    def describe_data(self, item_name: str = None, descr_format: desf = desf.MD):
        if self.implementation is not None:
            return self.implementation.describe_data(item_name, descr_format)

    @property
    def metadata(self):
        if self.implementation is not None:
            return self.implementation.metadata

    def describe_metadata(self, item_name: str = None, descr_format: desf = desf.MD):
        if self.implementation is not None:
            return self.implementation.describe_metadata(item_name, descr_format)

    @property
    def default_device_settings(self):
        if self.implementation is not None:
            return self.implementation.default_device_settings

    @property
    def json(self):
        if self.implementation is not None:
            return self.implementation.json

    @property
    def csv(self):
        if self.implementation is not None:
            return self.implementation.csv

    @property
    def well_names(self):  # Union(None, uniques)
        if self.implementation is not None:
            return self.implementation.well_names

    def dataframe_sorted(self, sort_criterion):
        if self.implementation is not None:
            return self.implementation.dataframe_sorted(sort_criterion)

    def get_semantics(self, attribute_name: str = None):
        """
        docstring
        """
        if self.implementation is not None:
            return self.implementation.get_semantics(attribute_name)


class NoDataFileError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return f"NoDataFileError: {self.message} "
        else:
            return f"NoDataFileError: no data file found."


class DataFormatIdError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return f"DataFormatIdError: {self.message} "
        else:
            return f"DataFormatIdError: data format with this ID found."
