"""_____________________________________________________________________

:PROJECT: LabDataReader

*units and conversion factors*

:details: units and conversion factors.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201021

________________________________________________________________________

"""

__version__ = "0.0.1"

from enum import Enum


class PysicalDimension(Enum):
    """
    docstring
    """
    LENGTH = 'length'
    MASS = 'mass'
    REL = "relative"
    TIME = "time"


class TimeUnits:
    """ Time Units - more constants than a real class ... """
    class YEARS:
        SYMBOL = 'y'
        TO_SI_FACTOR = 365.2425*24*60*60  # this is of course approximately
        FROM_SI_FACTOR = 1 / TO_SI_FACTOR

    class DAYS:
        SYMBOL = 'd'
        TO_SI_FACTOR = 24*60*60
        FROM_SI_FACTOR = 1 / TO_SI_FACTOR

    class HOURS:
        SYMBOL = 'h'
        TO_SI_FACTOR = 60*60
        FROM_SI_FACTOR = 1 / TO_SI_FACTOR

    class MINUTES:
        SYMBOL = 'min'
        TO_SI_FACTOR = 60
        FROM_SI_FACTOR = 1 / TO_SI_FACTOR

    class SECONDS:
        SYMBOL = 's'
        TO_SI_FACTOR = 1
        FROM_SI_FACTOR = 1

    class MILLISECOND:
        SYMBOL = 'ms'
        TO_SI_FACTOR = 1e-3
        FROM_SI_FACTOR = 1e3

    class MICROSECONDS:
        SYMBOL = 'us'
        TO_SI_FACTOR = 1e-6
        FROM_SI_FACTOR = 1e6

    class NANOSECONDS:
        SYMBOL = 'ns'
        TO_SI_FACTOR = 1e-9
        FROM_SI_FACTOR = 1e9

    class PICOSECONDS:
        SYMBOL = 'ps'
        TO_SI_FACTOR = 1e-12
        FROM_SI_FACTOR = 1e12

    class FEMTOSECONDS:
        SYMBOL = 'fs'
        TO_SI_FACTOR = 1e-15
        FROM_SI_FACTOR = 1e15
