"""_____________________________________________________________________

:PROJECT: LabDataReader

*data descriptor base class*

:details: data descriptor base class.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210110

.. note:: -
.. todo:: - conversion RDF-turtle in RDF-XML

________________________________________________________________________

"""

__version__ = "0.0.2"

from enum import Enum
import logging
from typing import Optional

from abc import ABC, abstractmethod


class DescriptionFormat(Enum):
    MD = "markdown"
    RDFX = "RDF-XML"
    TTL = "RDF-turtle"
    QUANT = "quantity"
    DIM = "dimension"
    UNIT = "unit"
    TYPE = "type"


class DataDescriptor(ABC):
    __slots__ = 'rdf_header', 'description_human', \
                'description_rdf_xml', 'description_rdf_ttl', \
                'standard_items'

    @property
    def key_set(self):
        """
        docstring
        """
        return set(self.standard_items.keys())

    @abstractmethod
    def description(self, descr_format: DescriptionFormat = DescriptionFormat.MD):
        """
        docstring
        """
        raise NotImplementedError

    @abstractmethod
    def description_method(self, descr_format: DescriptionFormat = DescriptionFormat.MD):
        raise NotImplementedError

    @abstractmethod
    def description_device_settings(self, descr_format: DescriptionFormat = DescriptionFormat.MD):
        raise NotImplementedError
