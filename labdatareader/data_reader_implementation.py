
"""_____________________________________________________________________

:PROJECT: LabDataReader

*data reader implementation class*

:details: data reader implementation class
          and interface to read a data file

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201025

________________________________________________________________________
"""

__version__ = "0.0.3"


import os
import sys
from pathlib import Path
import re
import logging
from typing import Optional
import chardet

from abc import ABC, abstractmethod

import pandas as pd

from labdatareader.data_descriptor import DataDescriptor
from labdatareader.data_descriptor import DescriptionFormat as desf


class DataReaderImplementation(ABC):
    """DataReaderImplementation base class.
       Please subclass this class to generate an implementation of a data reader (plugin).
       Please look at implementation examples in the methods directory.

    :param ABC: [description]
    :type ABC: [type]
    """
    descriptor = None

    def __init__(self,
                 data_path: Optional[str] = None,
                 data_filename_list: list = [],
                 header_map: Optional[dict] = None,
                 metadata_defaults: dict = {},
                 data_defaults: dict = {},
                 data_start_ends: Optional[list] = [(0, 0)]):

        self.data_path = data_path
        self._data_filename_list = data_filename_list

        self._data_start_ends_list = data_start_ends
        self._header_map = header_map
        self._metadata_defaults = metadata_defaults
        self._data_defaults = data_defaults
        self._delimiter = ","

        self._dataframe = pd.DataFrame()
        self._curr_dataframe = pd.DataFrame()
        self._metadata = None
        self.data_format_description_ = None
        self.data_begin_end_lines = []

    @property
    def data_filename_list(self):
        return self._data_filename_list

    @property
    def data_start_ends(self):
        """
            docstring
        """
        return self._data_start_ends_list

    @data_start_ends.setter
    def data_start(self, start_ends: list):
        """
        docstring
        """
        self._data_start_ends_list = start_ends

    @property
    def header_map(self):
        """
        docstring
        """
        return self._header_map

    @header_map.setter
    def header_map(self, header_map: dict):
        """
        docstring
        """
        self._header_map = header_map

    @property
    def metadata_defaults(self):
        """
        docstring
        """
        return self._metadata_defaults

    @metadata_defaults.setter
    def metadata_defaults(self, metadata: dict):
        """
        docstring
        """
        self._metadata_defaults = metadata

    @property
    def csv_delimiter(self):
        """
        docstring
        """
        return self._delimiter

    @csv_delimiter.setter
    def csv_delimiter(self, delimiter: str):
        """
        docstring
        """
        self._delimiter = delimiter

    @property
    def dataframe(self):
        """Provides read data converted into Pandas Dataframe

        :return: data-frame of parsed data
        :rtype: Pandas data frame
        """
        if self._dataframe.empty:
            self._read_all_data()

        # check, if dataframe is according to standard in description
        df_col_set = set(self._dataframe.columns)
        if self.descriptor.key_set.issubset(df_col_set):
            return self._dataframe
        else:
            if len(df_col_set) > len(self.descriptor.key_set):
                diff_set = str(df_col_set - self.descriptor.key_set)
            else:
                diff_set = str(self.descriptor.key_set - df_col_set)
            raise StandardDataframeIncompleteError(diff_set)

    @property
    def dataframe_by_well(self):
        """Conveniance method: Provides read data converted into Pandas Dataframe, already sorted by well.

        :return: data-frame of parsed data, sorted by well
        :rtype: Pandas data frame
        """
        if self._dataframe.empty:
            self._read_all_data()
        # check, if dataframe is according to standard in description
        df_col_set = set(self._dataframe.columns)
        if self.descriptor.key_set.issubset(df_col_set):
            return self._dataframe.sort_values(['row', 'col'])
        else:
            raise StandardDataframeIncompleteError(
                str(df_col_set - self.descriptor.key_set))

    @property
    def dataframe_by_time(self):
        """Conveniance method: Provides read data converted into Pandas Dataframe, already sorted by time.

        :return: data-frame of parsed data, sorted by time
        :rtype: Pandas data frame
        """
        if self._dataframe.empty:
            self._read_all_data()

        # check, if dataframe is according to standard in description
        df_col_set = set(self._dataframe.columns)
        if self.descriptor.key_set.issubset(df_col_set):
            return self._dataframe.sort_values("delta_time_ref_s")
        else:
            raise StandardDataframeIncompleteError(
                str(df_col_set - self.descriptor.key_set))

    @property
    def metadata(self):
        """Provides the meta-data

        :return: metadata
        :rtype: Metadata
        """
        if self._metadata is None:
            self._read_all_data()
        return self._metadata

    @property
    def well_names(self):  # Union(None, uniques)
        if self._dataframe.empty:
            codes, uniques = pd.factorize(
                self._dataframe["well_name"], sort=True)

            return uniques

    def dataframe_sorted(self, sort_criterion):
        if self._dataframe.empty:
            self._read_all_data()
        return self._dataframe.sort_values(sort_criterion)

    @property
    def json(self):
        if self._dataframe.empty:
            self._read_all_data()
        return self._dataframe.to_json()

    @property
    def csv(self):
        if self._dataframe.empty:
            self._read_all_data()
            # check, if dataframe is according to standard
            if set(self.descriptor.standard_items.keys()).issubset(set(self._dataframe.columns)):
                return self._dataframe.to_csv()
            else:
                raise StandardDataframeIncompleteError

    def _readlines_utf8(self, filename: str, remove_empty: bool = True) -> list:
        """
        conveniance method: try to read data as UTF-8
        in case of UnicodeDecodeError, detect character encoding
        decode and return list of lines
        """
        no_unicode_file = False
        try:
            with open(filename, 'r') as f_in:
                if remove_empty:
                    # reading with removing empty lines
                    lines = tuple(filter(None, (line.rstrip()
                                                for line in f_in)))
                else:
                    lines = f_in.readlines()
                return lines
        except UnicodeDecodeError as err:
            no_unicode_file = True
            logging.warning(
                f"{err}\nfile: {filename} has non-utf8 encoding, trying to convert ...")

        if no_unicode_file:
            # re-open os binary file

            with open(filename, 'rb') as fb_in:
                raw_data = fb_in.read()
                # detect encoding
                result = chardet.detect(raw_data)
                char_encoding = result['encoding']
                logging.info(f"encoding: {char_encoding}")
                # decode/convert and return as lines
                enc_lines = raw_data.decode(char_encoding).split('\n')

                if remove_empty:
                    return tuple(filter(None, (line.rstrip() for line in enc_lines)))
                else:
                    return enc_lines

    def read_csv(self, data_filename: str = ""):
        nrows = None
        header = 'infer'
        names = None

        # in case keys in map dictionary are integers, use values as column names
        if self._header_map is not None and type(list(self._header_map)[0]) == int:
            header = None
            names = [self._header_map[key] for key in sorted(self._header_map)]

        for data_start, data_end in self._data_start_ends_list:
            if data_end > 0:
                nrows = data_end - data_start
            try:
                temp_df = pd.read_csv(data_filename,
                                      delimiter=self._delimiter,
                                      header=header,
                                      names=names,
                                      skiprows=data_start, nrows=nrows)
            except UnicodeDecodeError:
                with open(data_filename, 'rb') as fb_in:
                    raw_data = fb_in.read()
                    # detect encoding
                    dect_result = chardet.detect(raw_data)

                    temp_df = pd.read_csv(data_filename,
                                          delimiter=self._delimiter,
                                          header=header,
                                          names=names,
                                          skiprows=data_start,
                                          nrows=nrows,
                                          encoding=dect_result['encoding'])

            if self._header_map is not None and type(list(self._header_map)[0]) != int:
                # apply the header map, but first remove white spaces from the column names
                temp_df.rename(
                    columns=lambda x: x.strip(), inplace=True)
                temp_df.rename(
                    columns=self._header_map, inplace=True)

            if self._curr_dataframe.empty:
                self._curr_dataframe = temp_df
            else:
                self._curr_dataframe = pd.concat([
                    self._curr_dataframe, temp_df], ignore_index=True)

    def add_default_df_col(self, column_name: str):
        """
        docstring
        """
        try:
            self._curr_dataframe[column_name] = self._data_defaults[column_name]
        except KeyError as err:
            logging.warning(
                f"WARNING({err}): No data for {column_name} provided, trying to load default ...")
            try:
                self._curr_dataframe[column_name] = self.descriptor.standard_items[column_name]['default']
            except KeyError as err:
                logging.error(
                    f"ERROR {err}:\n no default value found for item  [{column_name}]")

    def append_new_dataframe(self):
        if self._dataframe.empty:
            self._dataframe = self._curr_dataframe
        else:
            self._dataframe = pd.concat(
                [self._dataframe, self._curr_dataframe], ignore_index=True)

        self._curr_dataframe = pd.DataFrame()

    def search_metadata(self, md_attribute, line: str = "a", last_line: bool = True,
                        pattern: str = "b", info_position: int = None,
                        default=None) -> None:
        m = re.match(pattern, line)
        if m is not None:
            if md_attribute == "users":
                self._metadata[md_attribute] = [m[info_position]]
            else:
                self._metadata[md_attribute] = m[info_position]
        # visualisation handling
        elif default is None and md_attribute == "visualisation_default":
            self._metadata[md_attribute] = self.descriptor.visualisation_default
        elif default is None and md_attribute == "visualisations":
            self._metadata[md_attribute] = self.descriptor.visualisations
        elif default is not None and self._metadata is not None:
            self._metadata[md_attribute] = default
        elif last_line:
            try:
                self._metadata[md_attribute] = self._metadata_defaults[md_attribute]
            except Exception as err:
                # logging.warning(
                #    f"WARNING{err}:\n no default metadata value set by user: [{md_attribute}]")
                try:
                    self._metadata[md_attribute] = self._metadata.descriptor.standard_items[md_attribute]['default']
                except Exception as err:
                    logging.error(
                        f"ERROR {err}:\n no default metadata value found for item [{md_attribute}]")

    def _search_data_begin_end_lines(self, line_number: int,
                                     line: str, pattern: str,
                                     offset: int = 0):
        m = re.match(pattern, line)
        if m is not None:
            self.data_begin_end_lines.append([line_number + offset, 0])
            try:
                self.data_begin_end_lines[-2][1] = line_number
            except Exception as err:
                pass

    def _add_end_line(self, line_number):
        try:
            self.data_begin_end_lines[-1][1] = line_number + 1
        except Exception as err:
            pass

    @abstractmethod
    def _read_all_data(self):
        """
        generic internal data reader method
        this method needs to be implemented ! 
        """
        raise NotImplementedError

    def get_semantics(self, item_name: str = None, output_format: desf = desf.MD):
        """
        docstring
        """
        if self.descriptor is not None and item_name is not None:
            return self.descriptor.standard_items[item_name][output_format.value]
        else:
            pass
            # return full semantics

    def describe_method(self, method: str = None, descr_format: desf = desf.MD):
        if method is not None:
            raise NotImplementedError
        else:
            return self.descriptor.description_method()

    @property
    def default_device_settings(self):
        return self.descriptor.description_device_settings()

    def describe_data(self, item_name: str = None, descr_format: desf = desf.MD):
        """
        docstring
        """
        if self.descriptor is not None and item_name is not None:
            return self.descriptor.standard_items[item_name][descr_format.value]
        else:
            # describe everything
            return [(item_name, item_dict[descr_format.value]) for item_name, item_dict in self.descriptor.standard_items.items()]

    def describe_metadata(self, item_name: str = None, descr_format: desf = desf.MD):
        """
        docstring
        """
        if self._metadata.descriptor is not None and item_name is not None:
            return self._metadata.descriptor.standard_items[item_name][descr_format.value]
        else:
            # describe everything
            return [(item_name, item_dict[descr_format.value]) for item_name, item_dict in self._metadata.descriptor.standard_items.items()]


# ---- container well naming convenience methods

    def _std_well_name(self, row_name: str, col_name: str):
        """
        docstring
        """
        return f"{row_name}{int(col_name):02d}"

    def _std_well_num(self, col_num, row_num, max_col_num):
        """
        docstring
        """
        return col_num + (max_col_num * row_num)

    def _std_well_name2row_col(self, well_name: str):
        # all wells shall be also numbered from 0

        m = re.match(r"(^[a-zA-Z]*)(\d*)", well_name)
        if m is not None:
            row = m[1]
            col = int(m[2])
            return row, col

    def _std_well_name2row_col_num(self, well_name: str):
        # all wells shall be also numbered from 0
        # todo: support larger plates like 1536 !

        m = re.match(r"(^[a-zA-Z]*)(\d*)", well_name)
        if m is not None:
            row_num = ord(m[1]) - ord("A")
            col_num = int(m[2]) - 1
            return row_num, col_num

    def _std_well_name2well_num(self, well_name: str, max_col_num):
        """
        docstring
        todo: support larger plates like 1536 !
        """
        m = re.match(r"(^[a-zA-Z]*)(\d*)", well_name)
        if m is not None:
            row_num = ord(m[1]) - ord("A")
            col_num = int(m[2])
            return col_num + (max_col_num * row_num)


class StandardDataframeIncompleteError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None

    def __str__(self):
        if self.message:
            return f"StandardDataframeIncompleteError:\n"\
                f"The generated dataframe does not contain all standard columns:\n  [{self.message}]"
        else:
            return f"StandardDataframeIncompleteError: The generated dataframe does not contain all standard columns."
