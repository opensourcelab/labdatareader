# lara-django-data-importer to-do list

## Simulation module
- spectrum simulator
- kinetics simulator
- chromatogram simulator

## device support for
 - VariokanLux (xml)
  - SPabs
  - SPfl
  - KINabs
  - KINfl
  - AbsSpectrum
  
 - VariskanLux (skax)
 - Varioskan (txt)
 - BMG omega
 - TECAN Magellan

