"""_____________________________________________________________________

:PROJECT: LabDataReader

*Thermo Skanit2 UV-vis absorbance data importer in matrix format*

:details: Thermo Skanit2 UV-vis absorbance importer in matrix format.
          - importing of single (end) point and
          - kinetic measurements

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210206

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import sys
import logging

import pandas as pd

from labdatareader.metadata import MetaData, Plot2D
from labdatareader.data_reader_implementation import DataReaderImplementation
from labdatareader.methods.absorbance.absorbance_descriptor import AbsorbanceDataDescriptor


class DataReaderImpl(DataReaderImplementation):
    data_format_description = ("Thermo SkanIT 2.X absorbance list reader.\n"
                               "Absorbance measurements in the UV - vis spectroscopic range.\n"
                               "Single point as well as kinetic measurements can be read.\n")
    device_settings_description = """To reproduce the required data format, 
      please set up the device software with the following settings:

      *Output format*
       Matrix

      **Date and Time Format**
        YYYY-MM--DD hh:mm:ss  (24hr format, UTC+0)

      **Floating Point Values**
        - floating point separator: '.'

      **Barcode**
        The barcode should be written into ID1
    """

    descriptor = AbsorbanceDataDescriptor()
    descriptor.description_md = data_format_description
    descriptor.description_device_settings_md = device_settings_description

    def __init__(self, data_path: str = ".",
                 data_filename_list: list = [],
                 header_map=None, metadata_defaults={}, data_defaults={}, data_start_ends=[(0, 0)]):
        super().__init__(data_path=data_path,
                         data_filename_list=data_filename_list,
                         header_map=header_map,
                         metadata_defaults=metadata_defaults,
                         data_defaults=data_defaults,
                         data_start_ends=data_start_ends)

    # decide to read one single file or many in list

    def _read_all_data(self):
        self._metadata = MetaData()
        self._metadata.meas_temperatures = []
        self._metadata.meas_filenames = self.data_filename_list
        self._metadata.wavelengths = []
        self._metadata.timestamps = []

        for session_num, data_filename in enumerate(self.data_filename_list):
            self._read_metadata(data_filename, session_num)
            self._read_data(data_filename)

        self._dataframe["datetime_start"] = self._dataframe[
            "datetime"]  # pd.to_datetime(self._dataframe["datetime"])
        # reference time = smallest timepoint
        date_time_ref = self._dataframe["datetime_start"].min()

        self._dataframe["delta_datetime"] = self._dataframe[
            "datetime_start"] - date_time_ref
        # calculate time difference in seconds
        # relative to first measurement as reference, ignoring duration
        self._dataframe["delta_time_ref_s"] = self._dataframe["delta_datetime"].apply(
            lambda delta_datetime: delta_datetime.total_seconds())

    def _read_metadata(self, data_filename: str, session_num: int):
        last_line = False
        self.num_cycles = 0
        self.postioning_delay = 0.2

        content_ascii_lines = self._readlines_utf8(data_filename)

        # todo: handle first iteration separately

        # extract all  metatdata
        for i, line in enumerate(content_ascii_lines):
            if i == len(content_ascii_lines):
                last_line = True
            m = re.match(r"(^Date:)(.*)", line)
            if m is not None:
                if session_num == 1:
                    self._metadata.timestamp = datetime.strptime(
                        line.strip(), 'Date: %d/%m/%y  Time: %H:%M:%S %p')
                self._metadata.timestamps.append(datetime.strptime(
                    line.strip(), 'Date: %d/%m/%y  Time: %H:%M:%S %p'))
            else:
                self._metadata.timestamp = datetime.min

            # self.search_metadata("barcode", line,
            #                     r"(^ID1:\s*)(.*)ID2:\s(.*)ID3:\s(.*)", 2)
            self.search_metadata("meas_procedure_name", line, last_line,
                                 r"(^Testname:\s*)(.*)", 2)
            self.search_metadata("meas_software", line, last_line,
                                 r"(^Reader type:\s*)(.*)", 2)
            self.search_metadata("meas_software_version", line, last_line,
                                 r"(^Omega software version \(control part\):\s)(.*)", 2)
            self.search_metadata("device_version", line, last_line,
                                 r"(^Firmware version:\s*)(.*)", 2)
            self.search_metadata("device_serial", line, last_line,
                                 r"(^Reader serial number:\s*)(.*)", 2)
            self.search_metadata("users", line, last_line,
                                 r"(^BMG user:\s)(.*)", 2)

            # No. of Channels / Multichromatics:
            m = re.match(r"(^Positioning delay \[s\]:\s*)(\d*.\d*)(.*)", line)
            if m is not None:
                # might be necessary to store in list
                self.postioning_delay = float(m[2])

            # No. of Cycles:   - for kinetics in one file
            m = re.match(r"(^No\. of Cycles:\s*)(\d*)(.*)", line)
            if m is not None:
                # might be necessary to store in list
                self.num_cycles = int(m[2])

            m = re.match(r"(^\s*\d:\s*)(\d*)nm\s*(.*)", line)
            if m is not None:
                self._metadata.wavelengths.append(m[2])

            #  data_start_line_label  = r"Plate[\s]Well[\s]Type[\s]Sample"
            self._search_data_begin_end_lines(i, line, r"^Chromatic:\s*\d*", 4)

            # search current temperature
            m = re.match(r"(^T\[°C\]\s*:\s*)(\d*\.\d*)(.*)", line)
            if m is not None:
                self._metadata.meas_temperatures.append(m[2])
                logging.debug(f"temperature found: {m[2]} ")

        self._add_end_line(i)
        # metadata not specified in file
        self.search_metadata("method", default="absorbance")

        self._metadata.session_name = str(
            self._metadata.timestamp) + self._metadata.meas_procedure_name

        self._metadata.device_type = 'microtiter platereader'
        self._metadata.device_name = self._metadata.meas_software

        self.search_metadata("meas_procedure_name")
        self.search_metadata("timestamp")
        self.search_metadata("visualisation_default", default=Plot2D.XY)
        self.search_metadata("visualisations",
                             default=[Plot2D.XY, Plot2D.CHROMATOGRAM])

        self.search_metadata("session_name")
        self.search_metadata("device_name", default="generic")
        self.search_metadata("device_version", default=__version__)
        self.search_metadata("device_type", default="generic")
        self.search_metadata("device_serial")
        self.search_metadata("meas_software", default="generic-csv")
        self.search_metadata("meas_software_version")

        self.search_metadata("environment_temperature")
        self.search_metadata("environment_air_pressure")
        self.search_metadata("environment_air_humidity")
        self.search_metadata("users")
        self.search_metadata("geolocation")
        self.search_metadata("altitude")

        self.search_metadata("barcode")

    def _read_csv_data(self, data_filename: str):

        self.read_csv(data_filename)

        self.add_default_df_col('temperature')
        self.add_default_df_col('well_name')
        self.add_default_df_col('row')
        self.add_default_df_col('col')
        self.add_default_df_col('row_num')
        self.add_default_df_col('col_num')
        self.add_default_df_col('well_num')
        self.add_default_df_col('sample_name')
        self.add_default_df_col('sample_type')
        self.add_default_df_col('sample_group')
        self.add_default_df_col('wavelength')
        self.add_default_df_col('saturated')
        self.add_default_df_col('datetime_start')
        self.add_default_df_col('measurement_duration')
        self.add_default_df_col('measurement_start')
        self.add_default_df_col('datetime')

        self.append_new_dataframe()
