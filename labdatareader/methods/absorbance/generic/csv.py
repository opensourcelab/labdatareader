"""_____________________________________________________________________

:PROJECT: LabDataReader

*Generic UV-vis spectrometer data importer*

:details: Generic UV-vis spectrometer data importer.
          The generic UV-vis spectrometer importer can be used to
          read simple time-absorption value pairs in csv format.
          The motiviation for this reader is that it provides
          the output format in a standardised way and also metadata,
          so the same labDataReader interface can be used.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          2020111

.. note:: -
.. todo:: - header mapping
        - delimiter handling

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import sys
import logging

import pandas as pd

from labdatareader.metadata import MetaData, Plot2D
from labdatareader.data_reader_implementation import DataReaderImplementation
from labdatareader.methods.absorbance.absorbance_descriptor import AbsorbanceDataDescriptor


class DataReaderImpl(DataReaderImplementation):
    data_format_description = ("Generic absorbance csv reader.\n"
                               "Absorbance measurements in the UV - vis spectroscopic range.\n"
                               "Single point as well as kinetic measurements can be read.\n")
    device_settings_description = """To reproduce the required data format, 
      please set up the device software with the following settings:
      
      **CSV Format**
        - separator/delimiter: ','
        - descimal separator: '.'

      **Comments**
        - comment character: '#'
    """

    descriptor = AbsorbanceDataDescriptor()
    descriptor.description_md = data_format_description
    descriptor.description_device_settings_md = device_settings_description

    def __init__(self, data_path: str = ".",
                 data_filename_list: list = [],
                 header_map=None, metadata_defaults={}, data_defaults={}, data_start_ends=[(0, 0)]):
        super().__init__(data_path=data_path,
                         data_filename_list=data_filename_list,
                         header_map=header_map,
                         metadata_defaults=metadata_defaults,
                         data_defaults=data_defaults,
                         data_start_ends=data_start_ends)

    # decide to read one single file or many in list

    def _read_all_data(self):
        self._metadata = MetaData()
        # currently only one file is supported
        for data_filename in self.data_filename_list:
            self._add_meta_data()
            self._read_csv_data(data_filename)

        # self.dataframe_["datetime_start"] = self.dataframe_[
        #    "datetime"]  # pd.to_datetime(self.dataframe_["datetime"])
        # reference time = smallest timepoint
        #date_time_ref = self.dataframe_["datetime_start"].min()

        # self.dataframe_["delta_datetime"] = self.dataframe_[
        #    "datetime_start"] - date_time_ref
        # calculate time difference in seconds
        # relative to first measurement as reference, ignoring duration
        # self.dataframe_["delta_time_ref_s"] = self.dataframe_["delta_datetime"].apply(
        #    lambda delta_datetime: delta_datetime.total_seconds())

    def _add_meta_data(self):
        self.search_metadata("method", default="absorbance")
        self.search_metadata("meas_procedure_name")
        self.search_metadata("timestamp")
        self.search_metadata("visualisation_default", default=Plot2D.XY)
        self.search_metadata("visualisations",
                             default=[Plot2D.XY, Plot2D.CHROMATOGRAM])

        self.search_metadata("session_name")
        self.search_metadata("device_name", default="generic")
        self.search_metadata("device_version", default=__version__)
        self.search_metadata("device_type", default="generic")
        self.search_metadata("device_serial")
        self.search_metadata("meas_software", default="generic-csv")
        self.search_metadata("meas_software_version")

        self.search_metadata("environment_temperature")
        self.search_metadata("environment_air_pressure")
        self.search_metadata("environment_air_humidity")
        self.search_metadata("users")
        self.search_metadata("geolocation")
        self.search_metadata("altitude")

        self.search_metadata("barcode")

    def _read_csv_data(self, data_filename: str):

        self.read_csv(data_filename)

        self.add_default_df_col('temperature')
        self.add_default_df_col('well_name')
        self.add_default_df_col('row')
        self.add_default_df_col('col')
        self.add_default_df_col('row_num')
        self.add_default_df_col('col_num')
        self.add_default_df_col('well_num')
        self.add_default_df_col('sample_name')
        self.add_default_df_col('sample_type')
        self.add_default_df_col('sample_group')
        self.add_default_df_col('wavelength')
        self.add_default_df_col('saturated')
        self.add_default_df_col('datetime_start')
        self.add_default_df_col('measurement_duration')
        self.add_default_df_col('measurement_start')
        self.add_default_df_col('datetime')

        self.append_new_dataframe()
