"""_____________________________________________________________________

:PROJECT: LARAsuite

*Thermo Skanit6 data importer*

:details: Thermo Skanit6 data importer
    supported file formats:
    skanit 6.0 - 6.1 xml output

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20200628

.. note:: -
.. todo:: - meta data
          - temperature
          - num readings (for spectrum / kinetics)
          - skax format

________________________________________________________________________
"""

__version__ = "0.1.1"

import os
import sys
import csv
import re
import logging
from lxml import etree
from lxml import objectify

from datetime import datetime, timedelta

import pandas as pd
import numpy as np

from labdatareader.metadata import MetaData
from labdatareader.data_reader_implementation import DataReaderImplementation
from labdatareader.methods.absorbance.absorbance_descriptor import AbsorbanceDataDescriptor


class DataReaderImpl(DataReaderImplementation):
    data_format_description = "Thermo microtiter plate readers, with Skanit 6.X \
                                   Absorbance measurements in the UV - vis spectroscopic range. \
                                   Single point as well as kinetic measurements can be read."

    device_settings_description = """To reproduce the required data format, 
         please set up the device software with the following settings:
      
         **XML Format**
         - use output as "List"
         - select all metadata to output

        """
    descriptor = AbsorbanceDataDescriptor()
    descriptor.description_md = data_format_description
    descriptor.description_device_settings_md = device_settings_description

    def __init__(self, data_path: str = ".",
                 data_filename_list: list = [],
                 header_map=None, metadata_defaults={}, data_defaults={}, data_start_ends=[(0, 0)]):
        super().__init__(data_path=data_path,
                         data_filename_list=data_filename_list,
                         header_map=header_map,
                         metadata_defaults=metadata_defaults,
                         data_defaults=data_defaults,
                         data_start_ends=data_start_ends)

    def _read_all_data(self):
        data_lst = []
        self._metadata = MetaData()

        for data_filename in self.data_filename_list:
            self.tree = objectify.parse(data_filename)
            self.root = self.tree.getroot()  # = Sessions

            self._read_metadata()
            self._read_skanit6_xml_file(data_lst)

        self._dataframe = pd.DataFrame(data_lst)

        self._dataframe["datetime_start"] = self._dataframe[
            "datetime"]  # pd.to_datetime(self._dataframe["datetime"])
        # reference time = smallest timepoint
        date_time_ref = self._dataframe["datetime_start"].min()

        self._dataframe["delta_datetime"] = self._dataframe[
            "datetime_start"] - date_time_ref
        # calculate time difference in seconds
        # relative to first measurement as reference, ignoring duration
        self._dataframe["delta_time_ref_s"] = self._dataframe["delta_datetime"].apply(
            lambda delta_datetime: delta_datetime.total_seconds())

    def _read_metadata(self):

        session_info = self.root.Session.SessionInformation
        self.instrument_info = self.root.Session.InstrumentInformation

        self.session_datetime = datetime.strptime(self.root.Session.get(
            "time"), "%m/%d/%Y %H:%M:%S %p")  # SessionExecutedTime

        self.search_metadata("timestamp", default=self.session_datetime)

        self.search_metadata("method", default="absorbance")

        self.search_metadata("meas_procedure_name",
                             default=session_info.SessionName.text)
        self.search_metadata(
            "session_name", default=self.root.Session.get("name"))

        self.search_metadata("device_type", default='microtiter platereader')
        self.search_metadata(
            "device_version", default=self.instrument_info.ESWVersion.text)
        self.search_metadata(
            "device_name", default=self.instrument_info.Name.text)
        self.search_metadata(
            "device_serial", default=self.instrument_info.InstrumentSerialNumber)

        soft_ver = session_info.SoftwareVersion.text.split(',')

        self.search_metadata("meas_software", default=soft_ver[0].strip())
        self.search_metadata("meas_software_version",
                             default=soft_ver[1].strip())

        self.search_metadata("users", default=[session_info.SessionUser.text])

        try:
            self.temperature = float(
                self.instrument_info.InstrumentSetTemperature.text.split()[0])
        except AttributeError:
            self.temperature = np.nan

        self.search_metadata("environment_temperature",
                             default=self.temperature)

        # metadata not specified in file
        self.search_metadata("environment_air_pressure")
        self.search_metadata("environment_air_humidity")

        self.search_metadata("geolocation")
        self.search_metadata("altitude")

        self.search_metadata("barcode", default="")

    def col_as_matrix(self, col: str = ""):
        self._dataframe = self._dataframe.sort_values(['row', 'col'])

        num_cols = self._dataframe['col_num'].max() + 1
        num_rows = self._dataframe['row_num'].max() + 1

        return self._dataframe[col].to_numpy().reshape(num_cols, num_rows)

    def _read_skanit6_xml_file(self, data_lst):
        #logging.debug(f"growth file: {data_filename}")
        max_col_num = 12  # should be determined automatically
        wells = self.root.Session.ResultStep.Plate.Wells

        for well in wells.Coordinate:
            curr_row = well.get("row")
            row_num = ord(curr_row) - ord("A")
            curr_col = int(well.get("column"))
            col_num = curr_col - 1
            # "{}{:02d}".format(curr_row, curr_col)
            well_name = self._std_well_name(curr_row, curr_col)
            sample_name = well.Sample.get("name")
            sample_type = well.Sample.get("name")  # please fix
            sample_group = well.Sample.Groups.SampleGroup.get("name")

            # iterating through all results / wavelengths

            for i, result in enumerate(well.Result):
                data_row = dict(row=curr_row,
                                col=curr_col,
                                row_num=row_num,
                                col_num=col_num,
                                # all wells shall be also numbered from 0
                                well_num=self._std_well_num(
                                    col_num, row_num, max_col_num),
                                well_name=well_name,
                                sample_name=sample_name,
                                sample_type=sample_type,
                                sample_group=sample_group,
                                wavelength=float(result.get('wavelength_ex')),
                                value=float(result.get(
                                    'value').replace(' ', '')),
                                saturated=True if result.get(
                                    "saturated") == 'true' else False,
                                temperature=self.temperature,
                                # timedelta(seconds=self._dataframe['time'].max())
                                measurement_start=float(
                                    result.get("time")),  # please fix
                                measurement_duration=float(result.get("time")),
                                datetime=self.session_datetime)
                data_lst.append(data_row)
