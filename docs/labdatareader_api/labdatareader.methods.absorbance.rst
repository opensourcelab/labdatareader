labdatareader.methods.absorbance package
========================================

Subpackages
-----------

.. toctree::

   labdatareader.methods.absorbance.bmg_omega
   labdatareader.methods.absorbance.generic
   labdatareader.methods.absorbance.jasco
   labdatareader.methods.absorbance.thermo_skanit6

Submodules
----------

labdatareader.methods.absorbance.absorbance\_descriptor module
--------------------------------------------------------------

.. automodule:: labdatareader.methods.absorbance.absorbance_descriptor
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: labdatareader.methods.absorbance
   :members:
   :undoc-members:
   :show-inheritance:
