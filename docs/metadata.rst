Metadata 
=========

Metadata is Information that describes the provided data and gives more information about the process with which the data was generated.
It tells e.g. which device exactly was used for the measurement, when the measurement was done and who measured the data.

Core Metadata
---------------
Information that is should be provided to all data - if not provided, 
an exception will be raised.

timestamp
method
meas_procedure_name
session_name
meas_software
meas_software_version

device_type
device_name
device_version
device_serial

environment_temperature
environment_air_pressure
environment_air_humidity

user
geolocation
altitude


Optional Metadata
-------------------

Information that is specific to a certain sample, e.g., 

ID 
barcode

