Writing your own LabDataReader Plugins
========================================

The **LabDataReader** framework is very flexible and allows a fast implementation of 
new readers for legacy file formats.

It uses a **plugin mechanism** to find new reader plugins in 
the methods/[method-name]/[output-format]/ directories.
All plugins shall contain an implementation (subclass) of the 
LabDataReaderImplementation base class.

For inspiration, please look at implementations in the subdirectories of the methods folder. 

The reader implementation is selected by *dependency injection* in the data_reader.py module.
*Dependency injection* is used to decouple the reader implementation from the framework.

A standardised naming scheme is used to address a reader plugin:

**method.data_format.data_subformat**

The general measurement method of the data file is selected by the 'method' attribute, e.g. **"absorbance"** .
The data format is described by the measurement software that generated the data format and sub-data 
format, since measurement software often generate various subformats. A plate reader software, e.g., 
might produce a different output file format for each measurement type: **"bmg_omega.uv_vis"**.

Example:

.. code-block:: python

   DataReader(method='absorbance', data_format="bmg_omega.uv_vis")

Here are some recommended steps to build a new data reader plugin:

#. decide, what kind of *pysical measurement method* the data belongs to. 
   A **method** is a high-level term for the pysical measurement principle (e.g. absorption, HPLC, GC, MS, NMR, ...)

#. decide, which software generated the data output
   since the measurement software determins more frequently the data output format,
   this was chosen as next category over, e.g., the device name, since different software can be 
   used to meassure on one device.

#. decide, which sub-format it should belong
   many measurement software can generate for different measurement procedures, a reader module should be written for each procedure

#. put your module in the according directory structure under methods (see examples there)
   in the case of the BMG example this would be
   methods/absorbance/bmg_pherastar/uv_vis_singlepoint.py

#. the implementation is a subclass of the DataReaderImplementation base class. 
   Dependency injection requires the always the same class name: DataReaderImpl

#. XML data formats can be parsed using the lxml objectify modules

#. ASCII/UTF-8 files are required to be parsed with pattern finding libraries,
    e.g., with the regular expressions package reg (see examples in the methods subfolders)

#. please try to read as many meta data as possible from the data file (fill at least the metadata.core).

#. assign `self.dataframe_` to the read dataframe

#. for the column names of the dataframe, please use the column names as described in the descriptor.py file

Enjoy the standadized and rich functionality of the framework !