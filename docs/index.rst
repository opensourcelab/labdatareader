.. labDataReader documentation master file, created by
   sphinx-quickstart on Sat Dec  5 09:31:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to labDataReader's documentation!
=========================================

**labDataReader** is a plugin based python framework for reading and
converting laboratory device data, including **metadata** and **semantic
information / meaning**. As input formats many widely used (legacy) 
measurement data formats are supported or can be very easily added through a **plugin system**.
Supported output formats are **Pandas Dataframes**, JSON, csv and AnIML via the **AnIML converter**.


Installation
------------

For installation, please look at :doc:`installation/0_installation`

Basic Usage
------------

.. code-block:: python

    from labdatareader.data_reader import DataReader
    
    data_reader = DataReader(method='HPLC',
                         data_format="hitachi_chromaster.chromatogram_uv_vis",
                         data_path=".", 
                         filename_pattern="my_chromatography_data*.dat")
    my_dataframe = data_reader.dataframe   # standardised Pandas Data frame

See also jupyter notebook examples for further usage.

Metadata
----------
The LabDataReader package does not only provide the read data in a highly standardised mannor, but also 
set of standardised **Metadata**. For details, please refer to the :doc:`metadata` documentation.

Semantics
-----------

Furthermore the labDataReader package provides comprehensive description of the meaning or semantics of 
all individual items. This can be used,e.g.,  in SPARQL queries to relate the infromation to other concepts.
For details, please refer to the :doc:`semantics` documentation.


.. include:: supported_data_formats.rst


Development
------------

labDataReader is very modular via a plugin system.
Writing new plugins is rather simple:

:doc:`development/5_writing_data_reader_plugins`

For further information, please have a look at 
the development section: :doc:`development/0_development`


Project Version
----------------
.. include:: ../VERSION


Table of Contents
-------------------

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 2
   :caption: Contents:

   metadata
   semantics
   supported_data_formats
   installation/*
   development/*
   labdatareader_api/*
   changelog
   todo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
