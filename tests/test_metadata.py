"""_____________________________________________________________________

:PROJECT: LabDataReader

*MetaData Testsuite*

:details: MetaData Testsuite

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201022

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.metadata import MetaData, MetaDataIncompleteError


class MetaDataTestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'spectrophotometer', 'jasco')
        #self.abs_filename_pattern = "*SPabs96_600_660*.xml"

    def test_core(self):
        #logging.debug(f"read xml {self.varioskanLux_data_reader.data_file_list}")
        md = MetaData()
        md.core.users = ['Ben']
        self.assertEqual(md.core.users[0], 'Ben')
        # testing adding a wrong attribute
        with self.assertRaises(AttributeError):
            md.core.not_in_core = "not in core"
        # testing completeness
        with self.assertRaises(AttributeError):
            md.core.timestamp
        with self.assertRaises(MetaDataIncompleteError):
            for item in md.core.items():
                pass
        # adding default values to core and test them

        # for item in md.core.items():
        #     print(item)
        # self.assertEqual(md.core.meas_software_version, "def value")


if __name__ == '__main__':
    logging.basicConfig(
        format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
