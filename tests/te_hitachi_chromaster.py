"""_____________________________________________________________________

:PROJECT: LabDataReader

*HPLC Hitachi chromaster data importer*

:details: HPLC Hitachi chromaster data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20201208

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader

class HitachiChromasterTestCase(unittest.TestCase):
    def setUp(self):
        #print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
                                      'test_data', 'HPLC', 'hitachi_chromaster' ) 
        self.abs_filename_pattern = "allyl_*.asc"
        self.data_reader = DataReader(method='HPLC', 
                                    data_format="hitachi_chromaster.chromatogram_uv_vis",
                                    data_path=self.data_path, 
                                    filename_pattern=self.abs_filename_pattern)

    def test_read_metadata(self):
        pass
        #print(self.data_reader.metadata)
        #self.assertEqual(self.data_reader.metadata.core.method, 'HPLC') 

    def test_read_dataframe(self):
        logging.debug(f"read asc {self.data_reader.data_file_list}")
        test_filename = "allyl_suextr.asc"
        self.assertEqual(self.data_reader.data_file_list[0], 
                             os.path.join(self.data_path, test_filename) )

        hplc_df = self.data_reader.dataframe

        self.assertEqual(hplc_df.loc[0, "time"], 0.0 )
        self.assertEqual(hplc_df.loc[0, "value"], 5.79350 )

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()