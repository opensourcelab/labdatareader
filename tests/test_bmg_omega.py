"""_____________________________________________________________________

:PROJECT: LabDataReader

*Test BMG omega data importer*

:details: Test BMG omega data importer

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20202016

.. note:: -
.. todo:: - 

________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging

from labdatareader.data_reader import DataReader


class BMGOmegaTestCase(unittest.TestCase):
    def setUp(self):
        # print(os.getcwd())
        self.data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                      'test_data', 'absorbance', 'bmg_omega')

    def test_read_metadata(self):
        self.abs_filename_pattern = "BC_0021_*_utf8.DAT"
        self.bmg_omega_data_reader = DataReader(method='absorbance',
                                                data_format="bmg_omega.uv_vis",
                                                data_path=self.data_path,
                                                filename_pattern=self.abs_filename_pattern)
        omega_meta = self.bmg_omega_data_reader.metadata

        print("bmg core md:", omega_meta.list_core())
        print("bmgtest-time:", self.bmg_omega_data_reader.describe_data("datetime"))

        # for key, val in omega_meta.items():
        #    print(key, val)

        logging.debug(omega_meta.device_version)

        self.assertEqual(omega_meta.method, 'absorbance')
        self.assertEqual(omega_meta.device_type, 'microtiter platereader')
        self.assertEqual(omega_meta.meas_software, 'FLUOstar Omega')
        #self.assertEqual(omega_meta.meas_software_version, 'ver. 6.0.3.3')

    def test_read_data(self):
        self.abs_filename_pattern = "BC_0021_*_utf8.DAT"
        self.bmg_omega_data_reader = DataReader(method='absorbance',
                                                data_format="bmg_omega.uv_vis",
                                                data_path=self.data_path,
                                                filename_pattern=self.abs_filename_pattern)
        logging.debug(f"read xml {self.bmg_omega_data_reader.data_file_list}")
        test_filename = "BC_0021_20141106_020731_omega_SPabs_600_660_utf8.DAT"
        # self.assertEqual(self.bmg_omega_data_reader.data_file_list[1],
        #                 os.path.join(self.data_path, test_filename))

        omega_df = self.bmg_omega_data_reader.dataframe

        self.assertEqual(omega_df.loc[0, "value"], 0.1431)
        self.assertEqual(omega_df.loc[0, "delta_time_ref_s"], 8204.0)
        self.assertEqual(omega_df.loc[191, "value"], 0.0806)
        # 8223.0)
        self.assertEqual(omega_df.loc[191, "delta_time_ref_s"],  8223.0)

    def test_read_data_decoding(self):
        logging.debug(f"testing if decoding works correctly")
        self.abs_filename_pattern = "BC_0021_20141106_020731_*_ISO-8859-1.DAT"
        self.bmg_omega_data_reader = DataReader(method='absorbance',
                                                data_format="bmg_omega.uv_vis",
                                                data_path=self.data_path,
                                                filename_pattern=self.abs_filename_pattern)

        omega_df = self.bmg_omega_data_reader.dataframe

        # print(omega_df.head())
        # print(omega_df.tail())

        self.assertEqual(omega_df.loc[0, "value"], 0.1432)
        self.assertEqual(omega_df.loc[0, "delta_time_ref_s"], 0.0)
        self.assertEqual(omega_df.loc[191, "value"],  0.0908)
        self.assertEqual(omega_df.loc[191, "delta_time_ref_s"], 19.0)


if __name__ == '__main__':
    logging.basicConfig(
        format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()
