"""_____________________________________________________________________

:PROJECT: LabDataReader

*labDataReader setup *

:details: labDataReader.
           For installation, run:
             pip3 install .
             or  python3 setup.py install
           For development, run:
             pip3 install -e .

:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: 20201206

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.1.1"

import os
import sys

from setuptools import setup, find_packages

pkg_name = 'labdatareader'


def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read()


install_requires = ['wheel', 'pandas', 'lxml', 'chardet', 'python_docs_theme']
data_files = []

setup(name=pkg_name,
      version=__version__,
      description="""LabDataReader - is a plugin based python framework for reading and converting
                   laboratory device data, including metadata and semantic information.""",
      long_description=read(os.path.join('docs', 'README.rst')),
      author='mark doerr',
      author_email='mark.doerr@uni-greifswald.de',
      keywords=('lab automation, laboratory, instruments,'
                'experiments, evaluation, AnIML, semantics'),
      url='https://gitlab.com/opensourcelab/labdatareader',
      license='MIT',
      packages=find_packages(),
      install_requires=install_requires,
      test_suite='',
      classifiers=['License :: OSI Approved :: MIT',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.8',
                   'Programming Language :: Python :: 3.9',
                   'Topic :: Utilities',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Scientific/Engineering :: Information Analysis',
                   'Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator',
                   'Topic :: Scientific/Engineering :: Information Analysis'],
      include_package_data=True,
      data_files=data_files,
      )
